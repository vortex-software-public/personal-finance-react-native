import axios from 'axios';
import { Alert } from 'react-native';

const BASE_URL = 'https://run.mocky.io/v3';

let alert = false;

const API = (contentType = 'application/json') => {
  const instance = axios.create({
    baseURL: BASE_URL,
    timeout: 20000,
    headers: {
      'Content-Type': contentType,
    },
  });

  instance.interceptors.response.use(
    (response) => {
      return response;
    },
    async (error) => {
      if (error.message && error.message === 'Network Error') {
        if (!alert) {
          alert = true;
          return Alert.alert(
            'Network Error',
            'Please check your connection settings and try again.',
            [
              {
                text: 'Ok',
                onPress: () => {
                  alert = false;
                },
              },
            ],
            { cancelable: false },
          );
        }
      }
      return Promise.reject(error);
    },
  );

  return instance;
};

const setAuthorizationToken = (token) => {
  axios.defaults.headers.common.Authorization = `Bearer ${token}`;
};

export { API, setAuthorizationToken };
