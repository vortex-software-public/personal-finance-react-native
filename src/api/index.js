import { API } from './config';
import {
  BALANCE,
  CATEGORIES,
  CLIENTS,
  CREATE_CATEGORY,
  CREATE_CLIENT,
  CREATE_CURRENCY,
  CREATE_MOVEMENT,
  CURRENCIES,
  DELETE_CATEGORY,
  DELETE_CLIENT,
  DELETE_CURRENCY,
  DELETE_MOVEMENT,
  EXPENSES_BY_MONTHS_BY_CATEGORY,
  EXPENSES_BY_MONTHS_BY_DAY,
  EXPENSES_FORECAST,
  LOGIN,
  MOVEMENTS,
  PROFILE,
  REPORT_MOVEMENTS,
  UPDATE_CATEGORY,
  UPDATE_CLIENT,
  UPDATE_CURRENCY,
  UPDATE_MOVEMENT,
} from './paths';

const login = (data) => API().post(LOGIN, data);
const getCurrencies = () => API().get(CURRENCIES);
const getClients = () => API().get(CLIENTS);
const getCategories = () => API().get(CATEGORIES);

const createCurrency = (data) => API().post(CREATE_CURRENCY, data);
const updateCurrency = (data) => API().post(UPDATE_CURRENCY, data);
const deleteCurrency = (id) => API().delete(DELETE_CURRENCY, id);

const createClient = (data) => API().post(CREATE_CLIENT, data);
const updateClient = (data) => API().post(UPDATE_CLIENT, data);
const deleteClient = (id) => API().delete(DELETE_CLIENT, id);

const createCategory = (data) => API().post(CREATE_CATEGORY, data);
const updateCategory = (data) => API().post(UPDATE_CATEGORY, data);
const deleteCategory = (id) => API().delete(DELETE_CATEGORY, id);

const getBalance = () => API().get(BALANCE);
const getExpensesForecast = () => API().get(EXPENSES_FORECAST);
const getExpensesByMonthsByCategory = () =>
  API().get(EXPENSES_BY_MONTHS_BY_CATEGORY);
const getExpensesByMonthsByDay = () => API().get(EXPENSES_BY_MONTHS_BY_DAY);
const getReportMovements = () => API().get(REPORT_MOVEMENTS);

const getMovements = () => API().get(MOVEMENTS);

const getProfile = () => API().get(PROFILE);

const createMovement = (data) => API().post(CREATE_MOVEMENT, data);
const updateMovement = (data) => API().post(UPDATE_MOVEMENT, data);
const deleteMovement = (id) => API().delete(DELETE_MOVEMENT, id);

export {
  login,
  getCurrencies,
  getClients,
  getCategories,
  createCurrency,
  updateCurrency,
  createClient,
  updateClient,
  deleteClient,
  deleteCurrency,
  createCategory,
  updateCategory,
  deleteCategory,
  getBalance,
  getExpensesForecast,
  getExpensesByMonthsByCategory,
  getExpensesByMonthsByDay,
  getReportMovements,
  getMovements,
  getProfile,
  createMovement,
  updateMovement,
  deleteMovement,
};
