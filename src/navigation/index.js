import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import colors from '../assets/colors';
import adminBottomRoutes from './adminBottomRoutes';
import { SafeAreaMaterialTopTabBar } from '../components';
import {
  EditClient,
  Login,
  NewClient,
  NewCurrency,
  EditCurrency,
  NewMovement,
  EditMovement,
} from '../views';
import clientBottomRoutes from './clientBottomRoutes';
import categoryStack from './categoryStack';

const tabBaseConfig = {
  tabBarPosition: 'bottom',
  tabBarComponent: SafeAreaMaterialTopTabBar,
  tabBarOptions: {
    showIcon: true,
    activeTintColor: colors.WHITE,
    inactiveTintColor: colors.WHITE,
    indicatorStyle: {
      top: 0,
      backgroundColor: colors.WHITE,
    },
    style: {
      height: 69,
      backgroundColor: colors.BLUE,
    },
    tabStyle: {
      height: 69,
    },
  },
};

const clientBottomTabs = createMaterialTopTabNavigator(
  clientBottomRoutes,
  tabBaseConfig,
);

const ClientStack = createStackNavigator({
  Home: {
    screen: clientBottomTabs,
    navigationOptions: {
      headerShown: false,
    },
  },
  ...categoryStack,
  NewMovement: {
    screen: NewMovement,
    navigationOptions: {
      headerShown: false,
    },
  },
  EditMovement: {
    screen: EditMovement,
    navigationOptions: {
      headerShown: false,
    },
  },
});

const adminBottomTabs = createMaterialTopTabNavigator(
  adminBottomRoutes,
  tabBaseConfig,
);

const AdminStack = createStackNavigator({
  Home: {
    screen: adminBottomTabs,
    navigationOptions: {
      headerShown: false,
    },
  },
  NewClient: {
    screen: NewClient,
    navigationOptions: {
      headerShown: false,
    },
  },
  NewCurrency: {
    screen: NewCurrency,
    navigationOptions: {
      headerShown: false,
    },
  },
  EditCurrency: {
    screen: EditCurrency,
    navigationOptions: {
      headerShown: false,
    },
  },
  EditClient: {
    screen: EditClient,
    navigationOptions: {
      headerShown: false,
    },
  },
  ...categoryStack,
});

const AuthStack = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'Login',
  },
);

const router = createSwitchNavigator(
  {
    AuthStack,
    AdminStack,
    ClientStack,
  },
  {
    initialRouteName: 'AuthStack',
  },
);

const AppContainer = createAppContainer(router);

export default () => <AppContainer />;
