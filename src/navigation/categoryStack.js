import { EditCategory, NewCategory } from '../views';

const categoryStack = {
  NewCategory: {
    screen: NewCategory,
    navigationOptions: {
      headerShown: false,
    },
  },
  EditCategory: {
    screen: EditCategory,
    navigationOptions: {
      headerShown: false,
    },
  },
};

export default categoryStack;
