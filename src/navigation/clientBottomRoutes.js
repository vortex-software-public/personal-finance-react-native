import React from 'react';

import { BottomNavIcon, BottomNavText } from '../components';
import {
  categoriesIcon,
  balanceIcon,
  reportsIcon,
  newMovementIcon,
  profileIcon,
} from '../assets/images';
import { Balance, Categories, Movements, Reports, Profile } from '../views';

const renderIcon = (key) => {
  let icon;
  switch (key) {
    case 'Balance':
      icon = balanceIcon;
      break;
    case 'Reports':
      icon = reportsIcon;
      break;
    case 'Movements':
      icon = newMovementIcon;
      break;
    case 'Profile':
      icon = profileIcon;
      break;
    default:
      icon = categoriesIcon;
  }
  return <BottomNavIcon icon={icon} />;
};

const renderLabel = (key) => {
  return <BottomNavText text={key} />;
};

const bottomRouteConfigFor = (screen, key) => {
  return {
    screen,
    navigationOptions: {
      tabBarIcon: () => {
        return renderIcon(key);
      },
      tabBarLabel: () => {
        return renderLabel(key);
      },
      swipeEnabled: false,
    },
  };
};

const clientBottomRoutes = {
  Balance: bottomRouteConfigFor(Balance, 'Balance'),
  Reports: bottomRouteConfigFor(Reports, 'Reports'),
  Movements: bottomRouteConfigFor(Movements, 'Movements'),
  Categories: bottomRouteConfigFor(Categories, 'Categories'),
  Profile: bottomRouteConfigFor(Profile, 'Profile'),
};

export default clientBottomRoutes;
