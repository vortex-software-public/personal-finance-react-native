import React from 'react';

import { BottomNavIcon, BottomNavText } from '../components';
import { currenciesIcon, clientsIcon, categoriesIcon } from '../assets/images';
import { Categories, Currencies, Clients } from '../views';

const renderIcon = (key) => {
  let icon;
  switch (key) {
    case 'Currencies':
      icon = currenciesIcon;
      break;
    case 'Clients':
      icon = clientsIcon;
      break;
    default:
      icon = categoriesIcon;
  }
  return <BottomNavIcon icon={icon} />;
};

const renderLabel = (key) => {
  return <BottomNavText text={key} />;
};

const bottomRouteConfigFor = (screen, key) => {
  return {
    screen,
    navigationOptions: {
      tabBarIcon: () => {
        return renderIcon(key);
      },
      tabBarLabel: () => {
        return renderLabel(key);
      },
      swipeEnabled: false,
    },
  };
};

const adminBottomRoutes = {
  Currencies: bottomRouteConfigFor(Currencies, 'Currencies'),
  Clients: bottomRouteConfigFor(Clients, 'Clients'),
  Categories: bottomRouteConfigFor(Categories, 'Categories'),
};

export default adminBottomRoutes;
