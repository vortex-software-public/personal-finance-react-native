import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';

import colors from '../assets/colors';
import { arrowBackIcon } from '../assets/images';
import { NavigationInjectedProps, withNavigation } from 'react-navigation';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.BLUE,
    height: 69,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    color: colors.WHITE,
    textAlign: 'center',
  },
  icon: {
    width: 15,
    height: 15,
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    height: 69,
    width: 50,
    top: 0,
    left: 0,
  },
});

export interface Props extends NavigationInjectedProps {
  title: string;
  showGoBackButton?: boolean;
}

const Header: React.FC<Props> = ({
  title,
  showGoBackButton,
  navigation: { goBack },
}) => {
  const renderBackButton = () => {
    return (
      <TouchableOpacity onPress={() => goBack()} style={styles.iconContainer}>
        <Image source={arrowBackIcon} style={styles.icon} />
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        {showGoBackButton && renderBackButton()}
        <Text style={styles.title}>{title}</Text>
      </View>
    </SafeAreaView>
  );
};

Header.defaultProps = {
  showGoBackButton: false,
};

export default withNavigation(Header);
