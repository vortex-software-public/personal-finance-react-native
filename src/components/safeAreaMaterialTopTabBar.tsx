import React from 'react';
import { SafeAreaView } from 'react-native';
import { MaterialTopTabBar } from 'react-navigation-tabs';
import { MaterialTabBarProps } from 'react-navigation-tabs/lib/typescript/src/types';

const SafeAreaMaterialTopTabBar = (props: MaterialTabBarProps) => {
  return (
    <SafeAreaView style={{ backgroundColor: 'transparent' }}>
      <MaterialTopTabBar {...props} />
    </SafeAreaView>
  );
};

export default SafeAreaMaterialTopTabBar;
