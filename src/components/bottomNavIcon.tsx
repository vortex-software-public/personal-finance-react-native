import React from 'react';
import { Image, ImageSourcePropType, StyleSheet } from 'react-native';

import colors from '../assets/colors';

const styles = StyleSheet.create({
  image: {
    width: 20,
    height: 20,
    tintColor: colors.WHITE,
  },
});

export interface Props {
  icon: ImageSourcePropType;
}

const BottomNavIcon: React.FC<Props> = ({ icon }) => (
  <Image style={styles.image} source={icon} />
);

export default BottomNavIcon;
