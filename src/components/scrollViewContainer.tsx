import React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import colors from '../assets/colors';

const styles = StyleSheet.create({
  container: {
    paddingBottom: -400,
    marginHorizontal: 20,
    marginTop: 20,
  },
  backgroundWhite: {
    backgroundColor: colors.WHITE,
    height: '100%',
  },
});

export interface Props {
  children: React.ReactNode;
}

const ScrollViewContainer: React.FC<Props> = ({ children }) => (
  <SafeAreaView style={styles.backgroundWhite}>
    <KeyboardAwareScrollView
      enableOnAndroid
      extraHeight={90}
      extraScrollHeight={100}
      contentContainerStyle={styles.container}
    >
      {children}
    </KeyboardAwareScrollView>
  </SafeAreaView>
);

export default ScrollViewContainer;
