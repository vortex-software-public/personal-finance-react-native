import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import ImagePicker from 'react-native-image-picker';

import colors from '../assets/colors';
import { editIcon } from '../assets/images';
import { checkAllPermissions } from '../helpers';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    marginRight: 10,
    width: 125,
    height: 125,
  },
  photo: {
    width: 123,
    height: 123,
  },
  icon: {
    width: 20,
    height: 20,
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 35,
    height: 35,
    borderRadius: 100,
    backgroundColor: colors.PRIMARY_COLOR,
  },
});

export interface PhotoSource {
  uri: string;
  data: string;
}

export interface Props {
  photoSource: string;
  setPhoto: ({}: PhotoSource) => void;
}

const EditAvatar: React.FC<Props> = ({ photoSource = '', setPhoto }) => {
  const openCamera = async () => {
    const options = {
      // noData: true, // If true, disables the base64 data field from being generated (greatly improves performance on large photos)
      quality: 0.3,
    };

    const checkPermissions = await checkAllPermissions();

    if (checkPermissions) {
      ImagePicker.showImagePicker(options, (response) => {
        // console.log('Response = ', response);
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          // console.log('ImagePicker Error: ', response.error);
        } else {
          const { uri, data } = response;
          setPhoto({ uri, data });
        }
      });
    }
  };
  return (
    <View style={styles.container}>
      <Image
        resizeMode="cover"
        source={{ uri: photoSource }}
        style={styles.photo}
        borderRadius={100}
      />
      <TouchableOpacity onPress={openCamera} style={styles.iconContainer}>
        <Image source={editIcon} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
};

export default EditAvatar;
