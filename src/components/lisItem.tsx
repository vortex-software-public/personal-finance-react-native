import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import colors from '../assets/colors';
import IconButton from './iconButton';
import { editIcon, trashIcon } from '../assets/images';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: colors.PRIMARY_COLOR,
    marginBottom: 20,
  },
  title: {
    fontSize: 16,
    color: colors.BLACK_TEXT,
  },
  subtitle: {
    fontSize: 12,
    color: colors.GRAY_TEXT,
    marginTop: 5,
  },
  textsContainer: {
    flex: 1,
  },
  buttonsContainer: {
    flexDirection: 'row',
  },
});

export interface Props {
  item: {
    name: string;
    abreviature?: string;
  };
  onPressEdit: () => void;
  onPressDelete: () => void;
}

const LisItem: React.FC<Props> = ({
  item: { name, abreviature },
  onPressEdit,
  onPressDelete,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.textsContainer}>
        <Text style={styles.title}>{name}</Text>
        {abreviature && <Text style={styles.subtitle}>{abreviature}</Text>}
      </View>
      <View style={styles.buttonsContainer}>
        <IconButton icon={editIcon} onPress={onPressEdit} />
        <IconButton icon={trashIcon} onPress={onPressDelete} />
      </View>
    </View>
  );
};

export default LisItem;
