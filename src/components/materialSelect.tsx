import React, { useRef } from 'react';
import { TouchableOpacity, Image, Text, StyleSheet } from 'react-native';

import ModalView from './modalView';
import SelectOptionModal from './selectOptionModal';
import colors from '../assets/colors';
import { arrowDownIcon } from '../assets/images';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderColor: colors.PRIMARY_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  icon: {
    width: 30,
    height: 30,
    tintColor: colors.PRIMARY_COLOR,
  },
});

export interface OptionItem {
  id: number;
  name: string;
}

export interface Props {
  data: Array<OptionItem>;
  value: string;
  onSelect: (item: OptionItem) => void;
}

const MaterialSelect: React.FC<Props> = ({ data, value, onSelect }) => {
  const materialSelectRef = useRef(null);

  const openModal = () => {
    materialSelectRef.current.show();
  };

  const hideModal = () => {
    materialSelectRef?.current.close({
      then: () => {},
    });
  };

  const onSelectItem = (item: OptionItem) => {
    onSelect(item);
    hideModal();
  };

  const renderArrow = () => (
    <Image style={styles.icon} source={arrowDownIcon} />
  );

  return (
    <>
      <ModalView ref={materialSelectRef}>
        <SelectOptionModal
          onClose={hideModal}
          data={data}
          onPressItem={onSelectItem}
        />
      </ModalView>
      <TouchableOpacity onPress={openModal} style={styles.container}>
        <Text>{value}</Text>
        {renderArrow()}
      </TouchableOpacity>
    </>
  );
};

export default MaterialSelect;
