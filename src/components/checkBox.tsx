import React from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  TextStyle,
} from 'react-native';

import { doneIcon } from '../assets/images';
import colors from '../assets/colors';

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  text: {
    fontSize: 16,
    color: colors.BLACK_TEXT,
    alignSelf: 'center',
    marginRight: 10,
  },
  disabled: {
    width: 20,
    height: 20,
    backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
  },
  enabled: {
    width: 20,
    height: 20,
    backgroundColor: colors.GREEN,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export interface Props {
  text: string;
  checked: boolean;
  onPress: () => void;
  containerStyle?: TextStyle;
}

const CheckBox: React.FC<Props> = ({
  text,
  checked,
  onPress,
  containerStyle,
}) => (
  <TouchableOpacity
    activeOpacity={0.8}
    onPress={onPress}
    style={[styles.button, containerStyle]}
  >
    <Text style={styles.text}>{text}</Text>
    <View style={checked ? styles.enabled : styles.disabled}>
      <Image source={doneIcon} style={{ width: 15, height: 15 }} />
    </View>
  </TouchableOpacity>
);

CheckBox.defaultProps = {
  containerStyle: {},
};

export default CheckBox;
