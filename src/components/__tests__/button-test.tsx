import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { ActivityIndicator, TouchableOpacity } from 'react-native';

import Button from '../button';

describe('rendering a Button', () => {
  const onPressMock = jest.fn();
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<Button text="test" onPress={onPressMock} />);
  });

  it('should render a <TouchableOpacity />', () => {
    expect(wrapper.find(TouchableOpacity)).toHaveLength(1);
  });

  it('should be simulate onpress', () => {
    wrapper.find(TouchableOpacity).simulate('press');
    expect(onPressMock.mock.calls.length).toBe(1);
  });
});

describe('rendering loading button', () => {
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = shallow(<Button text="test" onPress={() => {}} loading={true} />);
  });

  it('should render a <ActivityIndicator />', () => {
    expect(wrapper.find(ActivityIndicator)).toHaveLength(1);
  });
});
