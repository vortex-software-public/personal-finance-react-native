import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { TextInput } from 'react-native';

import MaterialInput from '../materialInput';

describe('rendering a MaterialInput', () => {
  let inputValue = '';
  const onChangeMock = jest.fn();
  let wrapper: ShallowWrapper;
  beforeEach(() => {
    wrapper = shallow(
      <MaterialInput
        value={inputValue}
        onChangeValue={onChangeMock}
        label="Test"
        inputKey="testKey"
      />,
    );
  });

  it('should render a <TextInput />', () => {
    expect(wrapper.find(TextInput)).toHaveLength(1);
  });

  it('should be simulate onChangeText', () => {
    wrapper.find(TextInput).simulate('changeText', 'Test');
    expect(onChangeMock).toHaveBeenCalledWith('testKey', 'Test');
  });
});
