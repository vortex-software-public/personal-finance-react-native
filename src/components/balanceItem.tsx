import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import colors from '../assets/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: colors.PRIMARY_COLOR,
    marginBottom: 20,
  },
  accountNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  accountName: {
    fontSize: 16,
    color: colors.BLACK_TEXT,
  },
  currency: {
    fontSize: 12,
    color: colors.GRAY_TEXT,
  },
  balanceContainer: {
    alignItems: 'flex-end',
  },
  balance: {
    fontSize: 12,
    color: colors.BLACK_TEXT,
  },
  incomes: {
    fontSize: 11,
    color: colors.GRAY_TEXT,
  },
});

export interface Props {
  item: {
    account_name: string;
    currency: string;
    label_balance: string;
    label_total_ingresos: string;
    label_total_egresos: string;
  };
}

const BalanceItem: React.FC<Props> = ({
  item: {
    account_name,
    currency,
    label_balance,
    label_total_egresos,
    label_total_ingresos,
  },
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.accountNameContainer}>
        <Text style={styles.accountName}>{account_name}</Text>
        <Text style={styles.currency}>{`(${currency})`}</Text>
      </View>
      <View style={styles.balanceContainer}>
        <Text>{`Balance: $${label_balance}`}</Text>
        <Text
          style={styles.incomes}
        >{`Total revenues: $${label_total_ingresos}`}</Text>
        <Text
          style={styles.incomes}
        >{`Total expenses: $${label_total_egresos}`}</Text>
      </View>
    </View>
  );
};

export default BalanceItem;
