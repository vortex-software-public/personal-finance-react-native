import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

import colors from '../assets/colors';
import IconButton from './iconButton';
import { editIcon, trashIcon } from '../assets/images';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: colors.PRIMARY_COLOR,
    marginBottom: 10,
  },
  name: {
    fontSize: 16,
    color: colors.BLACK_TEXT,
  },
  email: {
    fontSize: 11,
    color: colors.GRAY_TEXT,
    marginTop: 5,
  },
  date_of_birth: {
    fontSize: 9,
    color: colors.GRAY_TEXT,
    marginTop: 5,
  },
  textsContainer: {
    flex: 1,
  },
  buttonsContainer: {
    flexDirection: 'row',
  },
  photoContainer: {
    alignSelf: 'flex-start',
    marginRight: 10,
    width: 42,
    height: 42,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: colors.PRIMARY_COLOR,
  },
  photo: {
    width: 40,
    height: 40,
  },
});

export interface Props {
  item: {
    id: number;
    name: string;
    email: string;
    date_of_birth: string;
    photo_url: string;
  };
  onPressEdit: () => void;
  onPressDelete: () => void;
}

const ClientItem: React.FC<Props> = ({
  item: { name, email, date_of_birth, photo_url },
  onPressEdit,
  onPressDelete,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.photoContainer}>
        <Image
          resizeMode="cover"
          source={{ uri: photo_url }}
          style={styles.photo}
          borderRadius={50}
        />
      </View>
      <View style={styles.textsContainer}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.email}>{email}</Text>
        <Text style={styles.date_of_birth}>{date_of_birth}</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <IconButton icon={editIcon} onPress={onPressEdit} />
        <IconButton icon={trashIcon} onPress={onPressDelete} />
      </View>
    </View>
  );
};

export default ClientItem;
