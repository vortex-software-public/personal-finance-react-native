import React from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  FlatList,
  Text,
} from 'react-native';

import SelectOptionItem from './selectOptionItem';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,.5)',
    justifyContent: 'center',
    paddingVertical: 100,
    paddingHorizontal: 30,
  },
  wrapper: {
    height: '100%',
    justifyContent: 'center',
  },
  viewContainer: {
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 18,
  },
  emptyText: {
    backgroundColor: 'white',
    padding: 10,
    fontSize: 16,
  },
});

export interface OptionItem {
  id: number;
  name: string;
}

export interface Props {
  data: Array<OptionItem>;
  onClose: () => void;
  onPressItem: (item: OptionItem) => void;
}

const SelectOptionModal: React.FC<Props> = ({ data, onClose, onPressItem }) => {
  return (
    <TouchableWithoutFeedback onPress={onClose}>
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <TouchableWithoutFeedback>
            <View style={styles.viewContainer}>
              <FlatList
                data={data}
                renderItem={({ item }) => (
                  <SelectOptionItem item={item} onPress={onPressItem} />
                )}
                keyExtractor={(item) => item.id.toString()}
                ListEmptyComponent={() => (
                  <Text style={styles.emptyText}>
                    No hay datos para mostrar.
                  </Text>
                )}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default SelectOptionModal;
