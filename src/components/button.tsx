import React from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  TextStyle,
  ViewStyle,
} from 'react-native';

import colors from '../assets/colors';
import globalStyles from '../assets/globalStyles';

const styles = StyleSheet.create({
  button: {
    padding: 10,
    marginVertical: 15,
    borderRadius: 5,
    backgroundColor: colors.PRIMARY_COLOR,
    width: '100%',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: colors.WHITE,
  },
  shadow: {
    ...globalStyles.shadow,
  },
});

export interface Props {
  text: string;
  onPress: () => void;
  loading?: boolean;
  textStyle?: TextStyle;
  buttonStyle?: ViewStyle;
}

const Button: React.FC<Props> = ({
  text,
  onPress,
  loading,
  textStyle,
  buttonStyle,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.button, styles.shadow, buttonStyle]}
      disabled={loading}
    >
      {loading ? (
        <ActivityIndicator size="small" color="#ccc" />
      ) : (
        <Text style={[styles.text, textStyle]}>{text}</Text>
      )}
    </TouchableOpacity>
  );
};

export default Button;
