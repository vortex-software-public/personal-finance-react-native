import React from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';

import { addIcon } from '../assets/images';
import colors from '../assets/colors';
import globalStyles from '../assets/globalStyles';

const styles = StyleSheet.create({
  button: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 20,
    backgroundColor: colors.PRIMARY_COLOR,
    borderRadius: 50,
    ...globalStyles.shadow,
  },
  icon: {
    width: 20,
    height: 20,
  },
});

export interface Props {
  onPress: () => void;
}

const FloatButton: React.FC<Props> = ({ onPress }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={onPress}
      style={styles.button}
    >
      <Image source={addIcon} style={styles.icon} />
    </TouchableOpacity>
  );
};

export default FloatButton;
