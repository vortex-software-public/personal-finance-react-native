import React from 'react';
import { StyleSheet, SafeAreaView, View } from 'react-native';

import colors from '../assets/colors';

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginTop: 20,
    paddingBottom: 40,
    flex: 1,
  },
  backgroundWhite: {
    backgroundColor: colors.WHITE,
    height: '100%',
  },
});

export interface Props {
  children: React.ReactNode;
}

const ScrollViewContainer: React.FC<Props> = ({ children }) => (
  <SafeAreaView style={styles.backgroundWhite}>
    <View style={styles.container}>{children}</View>
  </SafeAreaView>
);

export default ScrollViewContainer;
