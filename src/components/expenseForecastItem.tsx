import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

import colors from '../assets/colors';
import { arrowBalanceIcon } from '../assets/images';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: colors.PRIMARY_COLOR,
    marginBottom: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  accountNameContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  accountName: {
    fontSize: 16,
    color: colors.BLACK_TEXT,
  },
  subtitleContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  subtitle: {
    fontSize: 12,
    color: colors.BLACK_TEXT,
  },
  currency: {
    fontSize: 12,
    color: colors.GRAY_TEXT,
  },
  balanceContainer: {
    alignItems: 'flex-end',
  },
  icon: {
    width: 12,
    height: 12,
    tintColor: colors.GREEN,
  },
  iconRed: {
    tintColor: colors.ERROR,
    transform: [{ rotate: '180deg' }],
  },
});

export interface Props {
  item: {
    account_name: string;
    label_next_month: string;
    label_next_month_amount: string;
    currency: string;
    label_balance: string;
    label_last_month: string;
    label_last_month_amount: string;
    label_next_month_percent: string;
    label_two_months_ago: string;
    label_two_months_ago_amount: string;
  };
}

const ExpenseForecastItem: React.FC<Props> = ({
  item: {
    account_name,
    currency,
    label_next_month,
    label_next_month_amount,
    label_next_month_percent,
    label_last_month,
    label_last_month_amount,
    label_two_months_ago,
    label_two_months_ago_amount,
  },
}) => {
  const isNegative = () => label_next_month_percent.includes('-');

  return (
    <View style={styles.container}>
      <View style={styles.accountNameContainer}>
        <Text style={styles.accountName}>{account_name}</Text>
        <View style={styles.subtitleContainer}>
          <Text style={styles.subtitle}>
            {`${label_next_month}: ${label_next_month_amount} (${currency})`}
          </Text>
          <View style={styles.row}>
            <Text style={styles.subtitle}>{`${label_next_month_percent}`}</Text>
            <Image
              source={arrowBalanceIcon}
              style={[styles.icon, isNegative() && styles.iconRed]}
            ></Image>
          </View>
        </View>
      </View>
      <View style={styles.balanceContainer}>
        <View style={styles.row}>
          <Text style={styles.currency}>{`${label_last_month}: `}</Text>
          <Text style={styles.subtitle}>{`${label_last_month_amount}`}</Text>
          <Text style={styles.currency}>{` (${currency})`}</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.currency}>{`${label_two_months_ago}: `}</Text>
          <Text
            style={styles.subtitle}
          >{`${label_two_months_ago_amount}`}</Text>
          <Text style={styles.currency}>{` (${currency})`}</Text>
        </View>
      </View>
    </View>
  );
};

export default ExpenseForecastItem;
