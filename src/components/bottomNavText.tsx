import React from 'react';
import { Text, StyleSheet } from 'react-native';

import colors from '../assets/colors';

const styles = StyleSheet.create({
  text: {
    color: colors.WHITE,
    fontSize: 11,
  },
});

export interface Props {
  text: string;
  focused?: boolean;
}

const BottomNavText: React.FC<Props> = ({ text }) => {
  return (
    <Text numberOfLines={1} style={styles.text}>
      {text}
    </Text>
  );
};

export default BottomNavText;
