import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 1,
    marginHorizontal: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 14,
  },
});

export interface OptionItem {
  id: number;
  name: string;
}

export interface Props {
  item: OptionItem;
  onPress: (item: OptionItem) => void;
}

const SelectOptionItem: React.FC<Props> = ({ item, onPress }) => (
  <TouchableOpacity style={styles.item} onPress={() => onPress(item)}>
    <Text style={styles.title}>{item.name}</Text>
  </TouchableOpacity>
);

export default SelectOptionItem;
