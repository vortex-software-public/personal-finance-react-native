import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

import colors from '../assets/colors';
import { arrowBalanceIcon, editIcon, trashIcon } from '../assets/images';
import IconButton from './iconButton';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: colors.PRIMARY_COLOR,
    marginBottom: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  column: {
    flexDirection: 'column',
  },
  category: {
    fontSize: 16,
    color: colors.BLACK_TEXT,
  },
  type: {
    fontSize: 11,
    color: colors.GRAY_TEXT,
  },
  typeContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  date: {
    fontSize: 12,
    color: colors.LIGHT_GRAY_TEXT,
  },
  currency: {
    fontSize: 12,
    color: colors.GRAY_TEXT,
  },
  balanceContainer: {
    alignItems: 'center',
  },
  icon: {
    width: 12,
    height: 12,
    tintColor: colors.GREEN,
  },
  iconRed: {
    tintColor: colors.ERROR,
    transform: [{ rotate: '180deg' }],
  },
  grayText: {
    fontSize: 12,
    color: colors.GRAY_TEXT,
  },
  blackText: {
    fontSize: 12,
    color: colors.BLACK_TEXT,
  },
  buttonsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export interface Account {
  currency: string;
  balance: string;
}

export interface Props {
  item: {
    type: string;
    category: string;
    date: string;
    amount: string;
    currency: string;
    accounts?: Array<Account>;
    title?: string;
    description?: string;
  };
  showActions?: boolean;
  onPressEdit?: () => void;
  onPressDelete?: () => void;
}

const MovementItem: React.FC<Props> = ({
  item: {
    type,
    category,
    date,
    amount,
    currency,
    accounts,
    title,
    description,
  },
  showActions,
  onPressEdit,
  onPressDelete,
}) => {
  const isNegative = () => type === 'Egreso';

  const generateBalanceText = () => {
    const balanceText = accounts.map((account, index) => {
      return (
        <>
          {index !== 0 && <Text>+</Text>}
          <Text style={styles.blackText}>{`${account.balance} `}</Text>
          <Text>{`${account.currency} `}</Text>
        </>
      );
    });
    return balanceText;
  };

  const renderBalance = () => {
    return accounts.length > 0 ? (
      <View style={styles.balanceContainer}>
        <Text style={styles.grayText}>Balance: {generateBalanceText()}</Text>
      </View>
    ) : null;
  };

  const renderMovementText = () => {
    return title && description ? (
      <Text style={[styles.blackText, { marginTop: 5 }]}>
        {`${title} - `} <Text style={styles.grayText}>{description}</Text>
      </Text>
    ) : null;
  };

  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        <View style={styles.row}>
          <View style={styles.column}>
            {category && <Text style={styles.category}>{category}</Text>}
            <Text style={styles.date}>{date}</Text>
          </View>
          <View style={styles.column}>
            <View style={styles.typeContainer}>
              <Text style={styles.type}>{type}</Text>
              <Image
                source={arrowBalanceIcon}
                style={[styles.icon, isNegative() && styles.iconRed]}
              ></Image>
            </View>
            <Text style={styles.category}>
              {amount} <Text style={styles.currency}>{`(${currency})`}</Text>
            </Text>
          </View>
        </View>
        {renderBalance()}
        {renderMovementText()}
      </View>
      {showActions && (
        <View style={styles.buttonsContainer}>
          <IconButton icon={editIcon} onPress={onPressEdit} />
          <IconButton icon={trashIcon} onPress={onPressDelete} />
        </View>
      )}
    </View>
  );
};

export default MovementItem;
