import React, { useState } from 'react';

import { View } from 'react-native';
import ScrollViewContainer from '../scrollViewContainer';
import MaterialInput from '../materialInput';
import Button from '../button';
import { validateRequiredField } from '../../helpers';
import { MaterialSelect } from '../index';

export interface Movement {
  type: string;
  category: string;
  date: string;
  amount: string;
  currency: string;
  description: string;
  title: string;
  titleError: string;
  descriptionError: string;
  amountError: string;
}

export interface Props {
  movementData: Movement;
  setMovementData: ({}: Movement) => void;
  onPressSave: () => void;
  loading: boolean;
}

const MovementForm: React.FC<Props> = ({
  movementData,
  movementData: {
    type,
    category,
    amount,
    currency,
    description,
    titleError,
    descriptionError,
    amountError,
    title,
  },
  setMovementData,
  onPressSave,
  loading,
}) => {
  const [selectedCategory, setSelectedCategory] = useState(categories[0]);
  const [selectedCurrency, setSelectedCurrency] = useState(currencies[0]);
  const [selectedTransactionType, setSelectedTransactionType] = useState(
    transaction_types[0],
  );

  const handleOnChangeValue = (inputKey: string, value: string) => {
    setMovementData({
      ...movementData,
      [inputKey]: value,
      [`${inputKey}Error`]: '',
    });
  };

  const handleOnSelectCategory = (item) => {
    setSelectedCategory(item);
  };

  const handleOnSelectTransactionType = (item) => {
    setSelectedTransactionType(item);
  };

  const handleOnSelectCurrency = (item) => {
    setSelectedCurrency(item);
  };

  const validateFields = () => {
    const inputs = ['title', 'description', 'amount'];
    const values = {
      title,
      description,
      amount,
    };

    const errors = validateRequiredField(inputs, values);

    if (errors) {
      setMovementData({
        ...movementData,
        ...errors,
      });
    }

    return errors ? true : false;
  };

  const handleOnPressSave = () => {
    if (!validateFields()) {
      onPressSave();
    }
  };

  return (
    <ScrollViewContainer>
      <View style={{ paddingBottom: 100 }}>
        <MaterialInput
          label="Title"
          inputKey="title"
          value={title}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter the title"
          error={titleError}
        />
        <MaterialSelect
          data={categories}
          value={selectedCategory.name}
          onSelect={handleOnSelectCategory}
        />
        <MaterialSelect
          data={transaction_types}
          value={selectedTransactionType.name}
          onSelect={handleOnSelectTransactionType}
        />
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <View style={{ width: '45%' }}>
            <MaterialInput
              keyboardType="number-pad"
              label="Amount"
              inputKey="amount"
              value={amount}
              onChangeValue={handleOnChangeValue}
              placeholder="0,00"
              error={amountError}
            />
          </View>
          <View style={{ width: '45%' }}>
            <MaterialSelect
              data={currencies}
              value={selectedCurrency.name}
              onSelect={handleOnSelectCurrency}
            />
          </View>
        </View>
        <MaterialInput
          label="Description"
          inputKey="description"
          value={description}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter the description"
          error={descriptionError}
        />

        <Button text="SAVE" onPress={handleOnPressSave} loading={loading} />
      </View>
    </ScrollViewContainer>
  );
};

export default MovementForm;

const currencies = [
  {
    id: 1,
    name: 'Pesos',
    abreviature: 'ARS',
  },
  {
    id: 2,
    name: 'Dólares',
    abreviature: 'USD',
  },
];

const categories = [
  {
    id: 1,
    name: 'Food',
  },
  {
    id: 2,
    name: 'Clothes',
  },
  {
    id: 3,
    name: 'Party and alcohol',
  },
];
const transaction_types = [
  {
    id: 1,
    name: 'Egreso',
  },
  {
    id: 2,
    name: 'Ingreso',
  },
];
