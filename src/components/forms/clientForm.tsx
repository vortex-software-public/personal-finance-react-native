import React from 'react';
import { View } from 'react-native';

import ScrollViewContainer from '../scrollViewContainer';
import MaterialInput from '../materialInput';
import Button from '../button';
import { EditAvatar } from '../index';
import { validateRequiredField } from '../../helpers';

export interface Client {
  name: string;
  email: string;
  date_of_birth: string;
  photo_url: string;
  nameError: string;
  emailError: string;
  date_of_birthError: string;
}

export interface PhotoSource {
  uri: string;
  data: string;
}

export interface Props {
  clientData: Client;
  setClientData: ({}: Client) => void;
  onPressSave: () => void;
  loading: boolean;
  photoSource: PhotoSource;
  setPhotoSource: ({}: PhotoSource) => void;
}

const ClientForm: React.FC<Props> = ({
  clientData,
  clientData: {
    name,
    email,
    date_of_birth,
    nameError,
    emailError,
    date_of_birthError,
  },
  setClientData,
  onPressSave,
  loading,
  photoSource,
  setPhotoSource,
}) => {
  const handleOnChangeValue = (inputKey: string, value: string) => {
    setClientData({
      ...clientData,
      [inputKey]: value,
      [`${inputKey}Error`]: '',
    });
  };

  const validateFields = () => {
    const inputs = ['name', 'email', 'date_of_birth'];
    const values = {
      name,
      email,
      date_of_birth,
    };

    const errors = validateRequiredField(inputs, values);

    if (errors) {
      setClientData({
        ...clientData,
        ...errors,
      });
    }

    return errors ? true : false;
  };

  const handleOnPressSave = () => {
    if (!validateFields()) {
      onPressSave();
    }
  };

  return (
    <ScrollViewContainer>
      <View style={{ paddingBottom: 100 }}>
        <EditAvatar photoSource={photoSource.uri} setPhoto={setPhotoSource} />
        <MaterialInput
          label="Name"
          inputKey="name"
          value={name}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter your name"
          error={nameError}
        />
        <MaterialInput
          keyboardType="email-address"
          label="Email"
          inputKey="email"
          value={email}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter your email"
          error={emailError}
        />
        <MaterialInput
          label="Date of birth"
          inputKey="date_of_birth"
          value={date_of_birth}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter your date of birth"
          error={date_of_birthError}
        />
        <Button text="SAVE" onPress={handleOnPressSave} loading={loading} />
      </View>
    </ScrollViewContainer>
  );
};

export default ClientForm;
