import React from 'react';

import { View } from 'react-native';
import ScrollViewContainer from '../scrollViewContainer';
import MaterialInput from '../materialInput';
import Button from '../button';
import { validateRequiredField } from '../../helpers';

export interface Currency {
  name: string;
  abreviature: string;
  nameError: string;
  abreviatureError: string;
}

export interface Props {
  currencyData: Currency;
  setCurrencyData: ({}: Currency) => void;
  onPressSave: () => void;
  loading: boolean;
}

const CurrencyForm: React.FC<Props> = ({
  currencyData,
  currencyData: { name, abreviature, nameError, abreviatureError },
  setCurrencyData,
  onPressSave,
  loading,
}) => {
  const handleOnChangeValue = (inputKey: string, value: string) => {
    setCurrencyData({
      ...currencyData,
      [inputKey]: value,
      [`${inputKey}Error`]: '',
    });
  };

  const validateFields = () => {
    const inputs = ['name', 'abreviature'];
    const values = {
      name: currencyData.name,
      abreviature: currencyData.abreviature,
    };

    const errors = validateRequiredField(inputs, values);

    if (errors) {
      setCurrencyData({
        ...currencyData,
        ...errors,
      });
    }

    return errors ? true : false;
  };

  const handleOnPressSave = () => {
    if (!validateFields()) {
      onPressSave();
    }
  };

  return (
    <ScrollViewContainer>
      <View style={{ paddingBottom: 100 }}>
        <MaterialInput
          label="Name"
          inputKey="name"
          value={name}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter the name"
          error={nameError}
        />
        <MaterialInput
          label="Abreviature"
          inputKey="abreviature"
          value={abreviature}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter the abreviature"
          error={abreviatureError}
        />
        <Button text="SAVE" onPress={handleOnPressSave} loading={loading} />
      </View>
    </ScrollViewContainer>
  );
};

export default CurrencyForm;
