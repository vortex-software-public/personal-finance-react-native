import React from 'react';

import { View } from 'react-native';
import ScrollViewContainer from '../scrollViewContainer';
import MaterialInput from '../materialInput';
import Button from '../button';
import { validateRequiredField } from '../../helpers';

export interface Category {
  name: string;
  nameError: string;
}

export interface Props {
  categoryData: Category;
  setCategoryData: ({}: Category) => void;
  onPressSave: () => void;
  loading: boolean;
}

const CategoryForm: React.FC<Props> = ({
  categoryData,
  categoryData: { name, nameError },
  setCategoryData,
  onPressSave,
  loading,
}) => {
  const handleOnChangeValue = (inputKey: string, value: string) => {
    setCategoryData({
      ...categoryData,
      [inputKey]: value,
      [`${inputKey}Error`]: '',
    });
  };

  const validateFields = () => {
    const inputs = ['name'];
    const values = {
      name: categoryData.name,
    };

    const errors = validateRequiredField(inputs, values);

    if (errors) {
      setCategoryData({
        ...categoryData,
        ...errors,
      });
    }

    return errors ? true : false;
  };

  const handleOnPressSave = () => {
    if (!validateFields()) {
      onPressSave();
    }
  };

  return (
    <ScrollViewContainer>
      <View style={{ paddingBottom: 100 }}>
        <MaterialInput
          label="Name"
          inputKey="name"
          value={name}
          onChangeValue={handleOnChangeValue}
          placeholder="Enter the name"
          error={nameError}
        />
        <Button text="SAVE" onPress={handleOnPressSave} loading={loading} />
      </View>
    </ScrollViewContainer>
  );
};

export default CategoryForm;
