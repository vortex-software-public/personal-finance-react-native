const reportTypes = {
  EXPENSES_FORECAST: 'EXPENSES_FORECAST',
  EXPENSES_BY_MONTHS_BY_DAY: 'EXPENSES_BY_MONTHS_BY_DAY',
  MOVEMENTS: 'MOVEMENTS',
  EXPENSES_BY_MONTHS_BY_CATEGORY: 'EXPENSES_BY_MONTHS_BY_CATEGORY',
};

const reportOptions = [
  { id: 1, name: 'Proyección de gastos', key: reportTypes.EXPENSES_FORECAST },
  {
    id: 2,
    name: 'Gastos diarios por mes',
    key: reportTypes.EXPENSES_BY_MONTHS_BY_DAY,
  },
  { id: 3, name: 'Movimientos', key: reportTypes.MOVEMENTS },
  {
    id: 4,
    name: 'Gastos por categorias por mes',
    key: reportTypes.EXPENSES_BY_MONTHS_BY_CATEGORY,
  },
];

export { reportOptions, reportTypes };
