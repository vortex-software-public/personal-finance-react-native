import React, { useEffect, useState } from 'react';

import { Header } from '../components';
import ClientForm from '../components/forms/clientForm';
import { createClientAction, resetInfoAction } from '../state/client/actions';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationInjectedProps } from 'react-navigation';

export interface Props extends NavigationInjectedProps {}

const NewClient: React.FC<Props> = ({ navigation: { goBack } }) => {
  const [photoSource, setPhotoSource] = useState({
    uri: 'https://api.adorable.io/avatars/285/maxi.png',
    data: '',
  });

  const [clientData, setClientData] = useState({
    name: '',
    email: '',
    date_of_birth: '',
    photo_url: '',
    nameError: '',
    emailError: '',
    date_of_birthError: '',
  });

  const creatingClient = useSelector(
    (state) => state.clientReducer.creatingClient,
  );
  const successCreatingClient = useSelector(
    (state) => state.clientReducer.successCreatingClient,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successCreatingClient) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successCreatingClient]);

  const handleCreateCurrency = () => {
    const data = {
      name: clientData.name,
      email: clientData.email,
      date_of_birth: clientData.date_of_birth,
      photo_url: photoSource.data,
    };
    dispatch(createClientAction(data));
  };

  return (
    <>
      <Header title="New Client" showGoBackButton />
      <ClientForm
        clientData={clientData}
        setClientData={setClientData}
        photoSource={photoSource}
        setPhotoSource={setPhotoSource}
        onPressSave={handleCreateCurrency}
        loading={creatingClient}
      />
    </>
  );
};

export default NewClient;
