import React, { useEffect } from 'react';
import { FlatList, Text } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import { getBalanceAction } from '../state/reports/actions';
import { ViewContainer, Header, BalanceItem } from '../components';

export interface BalanceItem {
  id: string;
  account_name: string;
  currency: string;
  label_balance: string;
  label_total_ingresos: string;
  label_total_egresos: string;
}

const Balance = () => {
  const balance = useSelector((state) => state.reportsReducer.balance);
  const fetchingBalance = useSelector(
    (state) => state.reportsReducer.fetchingBalance,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    getBalance();
  }, []);

  const getBalance = () => {
    dispatch(getBalanceAction());
  };

  const renderItem = (item: BalanceItem) => <BalanceItem item={item} />;

  const keyExtractor = (item: BalanceItem) => `${item.id}`;

  return (
    <>
      <Header title="Balance" />
      <ViewContainer>
        <FlatList
          data={balance}
          keyExtractor={(item) => keyExtractor(item)}
          renderItem={({ item }) => renderItem(item)}
          ListEmptyComponent={() => {
            return !fetchingBalance ? <Text>No data to display.</Text> : null;
          }}
          refreshing={fetchingBalance}
          onRefresh={() => getBalance()}
        />
      </ViewContainer>
    </>
  );
};

export default Balance;
