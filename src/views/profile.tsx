import React, { useEffect, useState } from 'react';

import { Button, Header } from '../components';
import ClientForm from '../components/forms/clientForm';
import {
  updateClientAction,
  resetInfoAction,
  getProfileAction,
} from '../state/client/actions';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationInjectedProps } from 'react-navigation';

export interface Props extends NavigationInjectedProps {}

const Profile: React.FC<Props> = ({ navigation: { goBack } }) => {
  const [photoSource, setPhotoSource] = useState({
    uri: '',
    data: '',
  });

  const [clientData, setClientData] = useState({
    name: '',
    email: '',
    date_of_birth: '',
    photo_url: '',
    nameError: '',
    emailError: '',
    date_of_birthError: '',
  });

  const client = useSelector((state) => state.clientReducer.client);
  const fetchingClient = useSelector(
    (state) => state.clientReducer.fetchingClient,
  );

  const updatingClient = useSelector(
    (state) => state.clientReducer.updatingClient,
  );
  const successUpdatingClient = useSelector(
    (state) => state.clientReducer.successUpdatingClient,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProfileAction());
  }, []);

  useEffect(() => {
    if (client) {
      setClientData(client);
      setPhotoSource({ uri: client.photo_url, data: '' });
    }
  }, [client]);

  useEffect(() => {
    if (successUpdatingClient) {
      dispatch(resetInfoAction());
    }
  }, [successUpdatingClient]);

  const handleUpdateProfile = () => {
    const data = {
      name: clientData.name,
      email: clientData.email,
      date_of_birth: clientData.date_of_birth,
      photo_url: photoSource.data,
    };
    dispatch(updateClientAction(data));
  };

  return (
    <>
      <Header title="Profile" />
      {client && !fetchingClient && (
        <ClientForm
          clientData={clientData}
          setClientData={setClientData}
          photoSource={photoSource}
          setPhotoSource={setPhotoSource}
          onPressSave={handleUpdateProfile}
          loading={updatingClient}
        />
      )}
    </>
  );
};

export default Profile;
