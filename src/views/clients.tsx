import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ClientItem, FloatButton, Header, ViewContainer } from '../components';
import { Alert, FlatList, Text } from 'react-native';

import { deleteClientAction, getClientsAction } from '../state/client/actions';
import { NavigationInjectedProps } from 'react-navigation';

export interface Client {
  id: number;
  name: string;
  email: string;
  date_of_birth: string;
  photo_url: string;
}

export interface Props extends NavigationInjectedProps {}

const Clients: React.FC<Props> = ({ navigation: { navigate } }) => {
  const clients = useSelector((state) => state.clientReducer.clients);
  const fetchingClients = useSelector(
    (state) => state.clientReducer.fetchingClients,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    getClients();
  }, []);

  const getClients = () => {
    dispatch(getClientsAction());
  };

  const renderItem = (item: Client) => (
    <ClientItem
      item={item}
      onPressDelete={() => handleDeleteClient(item.id)}
      onPressEdit={() => handleOnPressEdit(item)}
    />
  );

  const deleteClient = (id: number) => {
    dispatch(deleteClientAction(id));
    getClients();
  };

  const handleDeleteClient = (id: number) => {
    Alert.alert('Delete client', 'Are you sure?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'Ok',
        onPress: () => deleteClient(id),
      },
    ]);
  };

  const keyExtractor = (item: Client) => `${item.id}`;

  const handleNewClient = () => {
    navigate('NewClient');
  };

  const handleOnPressEdit = (item: Client) => {
    navigate('EditClient', { client: item });
  };

  return (
    <>
      <Header title="Clients" />
      <ViewContainer>
        <FlatList
          data={clients}
          keyExtractor={(item) => keyExtractor(item)}
          renderItem={({ item }) => renderItem(item)}
          ListEmptyComponent={() => {
            return !fetchingClients ? <Text>No data to display.</Text> : null;
          }}
          refreshing={fetchingClients}
          onRefresh={() => getClients()}
        />
      </ViewContainer>
      <FloatButton onPress={handleNewClient} />
    </>
  );
};

export default Clients;
