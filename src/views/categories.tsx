import React, { useEffect } from 'react';

import { FloatButton, Header, LisItem, ViewContainer } from '../components';
import { Alert, FlatList, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  deleteCategoryAction,
  getCategoriesAction,
} from '../state/category/actions';

export interface Category {
  id: number;
  name: string;
}

export interface Props {
  navigation: {
    navigate: (routeName: string, params?: {}) => void;
  };
}
const Categories: React.FC<Props> = ({ navigation: { navigate } }) => {
  const categories = useSelector((state) => state.categoryReducer.categories);
  const fetchingCategories = useSelector(
    (state) => state.categoryReducer.fetchingCategories,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    getCategories();
  }, []);

  const getCategories = () => {
    dispatch(getCategoriesAction());
  };

  const renderItem = (item: Category) => (
    <LisItem
      item={item}
      onPressDelete={() => handleDeleteCategory(item.id)}
      onPressEdit={() => handleEditCategory(item)}
    />
  );

  const deleteCategory = (id: number) => {
    dispatch(deleteCategoryAction(id));
    getCategories();
  };

  const handleDeleteCategory = (id: number) => {
    Alert.alert('Delete category', 'Are you sure?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'Ok',
        onPress: () => deleteCategory(id),
      },
    ]);
  };

  const keyExtractor = (item: Category) => `${item.id}`;

  const handleEditCategory = (item: Category) => {
    navigate('EditCategory', { category: item });
  };

  const HandleNewCategory = () => {
    navigate('NewCategory');
  };

  return (
    <>
      <Header title="Categories" />
      <ViewContainer>
        <FlatList
          data={categories}
          keyExtractor={(item) => keyExtractor(item)}
          renderItem={({ item }) => renderItem(item)}
          ListEmptyComponent={() => {
            return !fetchingCategories ? (
              <Text>No data to display.</Text>
            ) : null;
          }}
          refreshing={fetchingCategories}
          onRefresh={() => getCategories()}
        />
      </ViewContainer>
      <FloatButton onPress={HandleNewCategory} />
    </>
  );
};

export default Categories;
