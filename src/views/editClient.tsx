import React, { useEffect, useState } from 'react';

import { Header } from '../components';
import ClientForm from '../components/forms/clientForm';
import { updateClientAction, resetInfoAction } from '../state/client/actions';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationInjectedProps } from 'react-navigation';

export interface Props extends NavigationInjectedProps {}

const NewClient: React.FC<Props> = ({ navigation: { goBack, getParam } }) => {
  const [photoSource, setPhotoSource] = useState({
    uri: '',
    data: '',
  });

  const [clientData, setClientData] = useState({
    name: '',
    email: '',
    date_of_birth: '',
    photo_url: '',
    nameError: '',
    emailError: '',
    date_of_birthError: '',
  });

  useEffect(() => {
    const client = getParam('client');
    setClientData(client);
    setPhotoSource({ uri: client.photo_url, data: '' });
  }, []);

  const updatingClient = useSelector(
    (state) => state.clientReducer.updatingClient,
  );
  const successUpdatingClient = useSelector(
    (state) => state.clientReducer.successUpdatingClient,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successUpdatingClient) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successUpdatingClient]);

  const handleCreateCurrency = () => {
    const data = {
      name: clientData.name,
      email: clientData.email,
      date_of_birth: clientData.date_of_birth,
      photo_url: photoSource.data,
    };
    dispatch(updateClientAction(data));
  };

  return (
    <>
      <Header title="New Client" showGoBackButton />
      <ClientForm
        clientData={clientData}
        setClientData={setClientData}
        photoSource={photoSource}
        setPhotoSource={setPhotoSource}
        onPressSave={handleCreateCurrency}
        loading={updatingClient}
      />
    </>
  );
};

export default NewClient;
