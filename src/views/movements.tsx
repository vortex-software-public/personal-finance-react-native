import React, { useEffect } from 'react';
import { Alert, FlatList, Text } from 'react-native';

import { useDispatch, useSelector } from 'react-redux';
import {
  ViewContainer,
  Header,
  MovementItem,
  FloatButton,
} from '../components';
import {
  deleteMovementAction,
  getMovementsAction,
} from '../state/movements/actions';
import { NavigationInjectedProps } from 'react-navigation';

export interface Movement {
  id: number;
  type: string;
  category: string;
  date: string;
  amount: string;
  currency: string;
  title: string;
  description: string;
}

export interface Props extends NavigationInjectedProps {}

const Movements = ({ navigation: { navigate } }) => {
  const movements = useSelector((state) => state.movementsReducer.movements);
  const fetchingMovements = useSelector(
    (state) => state.movementsReducer.fetchingMovements,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    getMovements();
  }, []);

  const getMovements = () => {
    dispatch(getMovementsAction());
  };

  const deleteMovement = (id: number) => {
    dispatch(deleteMovementAction(id));
    getMovements();
  };

  const handleDeleteMovement = (id: number) => {
    Alert.alert('Delete movement', 'Are you sure?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'Ok',
        onPress: () => deleteMovement(id),
      },
    ]);
  };

  const handleEditMovement = (item: Movement) => {
    navigate('EditMovement', { movement: item });
  };

  const renderItem = (item: Movement) => (
    <MovementItem
      item={item}
      showActions
      onPressEdit={() => handleEditMovement(item)}
      onPressDelete={handleDeleteMovement}
    />
  );

  const keyExtractor = (item: Movement) => `${item.id}`;

  const handleNewMovement = () => {
    navigate('NewMovement');
  };

  return (
    <>
      <Header title="Movements" />
      <ViewContainer>
        <FlatList
          data={movements}
          keyExtractor={(item) => keyExtractor(item)}
          renderItem={({ item }) => renderItem(item)}
          ListEmptyComponent={() => {
            return !fetchingMovements ? <Text>No data to display.</Text> : null;
          }}
          contentContainerStyle={{ paddingBottom: 50 }}
          refreshing={fetchingMovements}
          onRefresh={() => getMovements()}
        />
      </ViewContainer>
      <FloatButton onPress={handleNewMovement} />
    </>
  );
};

export default Movements;
