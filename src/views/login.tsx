import React, { useEffect, useState } from 'react';
import { Image } from 'react-native';

import {
  CheckBox,
  MaterialInput,
  Button,
  ScrollViewContainer,
} from '../components';
import { logo } from '../assets/images';
import { useDispatch, useSelector } from 'react-redux';
import { loginAction } from '../state/user/actions';
import { validateRequiredField } from '../helpers';

export interface Props {
  navigation: {
    navigate: (routeName: string) => void;
  };
}

const Login: React.FC<Props> = ({ navigation: { navigate } }) => {
  const fetchingLogin = useSelector((state) => state.userReducer.fetchingLogin);
  const token = useSelector((state) => state.userReducer.token);
  const dispatch = useDispatch();

  const [isAdmin, setIsAdmin] = useState(false);
  const [userData, setUserData] = useState({
    user: '',
    password: '',
    userError: '',
    passwordError: '',
  });

  const handleOnChangeValue = (inputKey: string, value: string) => {
    setUserData({
      ...userData,
      [inputKey]: value,
      userError: '',
      passwordError: '',
    });
  };

  useEffect(() => {
    if (token) {
      if (isAdmin) {
        navigate('AdminStack');
      } else {
        navigate('ClientStack');
      }
    }
  }, [token]);

  const validateFields = () => {
    const inputs = ['user', 'password'];
    const values = { user: userData.user, password: userData.password };

    const errors = validateRequiredField(inputs, values);

    if (errors) {
      setUserData({
        ...userData,
        ...errors,
      });
    }

    return errors ? true : false;
  };

  const handleOnPressCheckBox = () => setIsAdmin(!isAdmin);

  const handleLogin = () => {
    if (!validateFields()) {
      const data = { user: userData.user, password: userData.password };
      dispatch(loginAction(data, isAdmin));
    }
  };

  return (
    <ScrollViewContainer>
      <Image
        source={logo}
        style={{ width: 200, height: 200, alignSelf: 'center' }}
      />
      <MaterialInput
        label="User"
        inputKey="user"
        value={userData.user}
        onChangeValue={handleOnChangeValue}
        placeholder="Enter your username"
        error={userData.userError}
      />
      <MaterialInput
        label="Pass"
        inputKey="password"
        value={userData.password}
        onChangeValue={handleOnChangeValue}
        placeholder="Enter your password"
        showVisibilityIcon
        error={userData.passwordError}
      />
      <CheckBox
        checked={isAdmin}
        text="Login as admin"
        onPress={handleOnPressCheckBox}
        containerStyle={{ alignSelf: 'flex-end' }}
      />
      <Button text="LOGIN" onPress={handleLogin} loading={fetchingLogin} />
    </ScrollViewContainer>
  );
};

export default Login;
