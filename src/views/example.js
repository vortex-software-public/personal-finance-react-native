import React from 'react';
import { Text } from 'react-native';

import { ScrollViewContainer } from '../components';

const Example = () => {
  return (
    <ScrollViewContainer>
      <Text>Example</Text>
    </ScrollViewContainer>
  );
};

export default Example;
