import React, { useEffect, useState } from 'react';

import { Header } from '../components';
import MovementForm from '../components/forms/movementForm';
import { NavigationInjectedProps } from 'react-navigation';
import { useDispatch, useSelector } from 'react-redux';
import {
  resetInfoAction,
  updateMovementAction,
} from '../state/movements/actions';

export interface Props extends NavigationInjectedProps {}

const EditMovement: React.FC<Props> = ({
  navigation: { getParam, goBack },
}) => {
  const [movementData, setMovementData] = useState({
    type: '',
    category: '',
    date: '',
    amount: '',
    currency: '',
    title: '',
    description: '',
    titleError: '',
    descriptionError: '',
    amountError: '',
  });

  const updatingMovement = useSelector(
    (state) => state.movementsReducer.updatingMovement,
  );
  const successUpdatingMovement = useSelector(
    (state) => state.movementsReducer.successUpdatingMovement,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successUpdatingMovement) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successUpdatingMovement]);

  const handleEditMovement = () => {
    const data = {
      name: movementData.name,
    };
    dispatch(updateMovementAction(data));
  };

  useEffect(() => {
    const movement = getParam('movement');
    setMovementData({ ...movementData, ...movement });
  }, []);

  return (
    <>
      <Header title="Edit Movement" showGoBackButton />
      <MovementForm
        movementData={movementData}
        setMovementData={setMovementData}
        onPressSave={handleEditMovement}
        loading={updatingMovement}
      />
    </>
  );
};

export default EditMovement;
