import React, { useEffect, useState } from 'react';

import { Header } from '../components';
import CurrencyForm from '../components/forms/currencyForm';
import { NavigationInjectedProps } from 'react-navigation';
import {
  resetInfoAction,
  updateCurrencyAction,
} from '../state/currency/actions';
import { validateRequiredField } from '../helpers';
import { useDispatch, useSelector } from 'react-redux';

export interface Props extends NavigationInjectedProps {}

const EditCurrency: React.FC<Props> = ({
  navigation: { getParam, goBack },
}) => {
  const [currencyData, setCurrencyData] = useState({
    name: '',
    abreviature: '',
    nameError: '',
    abreviatureError: '',
  });

  const updatingCurrency = useSelector(
    (state) => state.currencyReducer.updatingCurrency,
  );
  const successUpdatingCurrency = useSelector(
    (state) => state.currencyReducer.successUpdatingCurrency,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successUpdatingCurrency) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successUpdatingCurrency]);

  const validateFields = () => {
    const inputs = ['name', 'abreviature'];
    const values = {
      name: currencyData.name,
      abreviature: currencyData.abreviature,
    };

    const errors = validateRequiredField(inputs, values);

    if (errors) {
      setCurrencyData({
        ...currencyData,
        ...errors,
      });
    }

    return errors ? true : false;
  };

  const handleEditCurrency = () => {
    if (!validateFields()) {
      const data = {
        name: currencyData.name,
        abreviature: currencyData.abreviature,
      };
      dispatch(updateCurrencyAction(data));
    }
  };

  useEffect(() => {
    const currency = getParam('currency');
    setCurrencyData(currency);
  }, []);

  return (
    <>
      <Header title="Edit Currency" showGoBackButton />
      <CurrencyForm
        currencyData={currencyData}
        setCurrencyData={setCurrencyData}
        onPressSave={handleEditCurrency}
        loading={updatingCurrency}
      />
    </>
  );
};

export default EditCurrency;
