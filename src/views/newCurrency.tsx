import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationInjectedProps } from 'react-navigation';

import { Header } from '../components';
import CurrencyForm from '../components/forms/currencyForm';
import {
  createCurrencyAction,
  resetInfoAction,
} from '../state/currency/actions';

export interface Props extends NavigationInjectedProps {}

const NewCurrency: React.FC<Props> = ({ navigation: { goBack } }) => {
  const [currencyData, setCurrencyData] = useState({
    name: '',
    abreviature: '',
    nameError: '',
    abreviatureError: '',
  });
  const creatingCurrency = useSelector(
    (state) => state.currencyReducer.creatingCurrency,
  );
  const successCreatingCurrency = useSelector(
    (state) => state.currencyReducer.successCreatingCurrency,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successCreatingCurrency) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successCreatingCurrency]);

  const handleCreateCurrency = () => {
    const data = {
      name: currencyData.name,
      abreviature: currencyData.abreviature,
    };
    dispatch(createCurrencyAction(data));
  };

  return (
    <>
      <Header title="New Currency" showGoBackButton />
      <CurrencyForm
        currencyData={currencyData}
        setCurrencyData={setCurrencyData}
        onPressSave={handleCreateCurrency}
        loading={creatingCurrency}
      />
    </>
  );
};

export default NewCurrency;
