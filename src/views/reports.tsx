import React, { useEffect, useState } from 'react';

import {
  ScrollViewContainer,
  MaterialSelect,
  ExpenseForecastItem,
  MovementItem,
  Header,
} from '../components';
import { reportOptions, reportTypes } from '../constants';
import {
  getExpensesByMonthsByCategoryAction,
  getExpensesForecastAction,
  getExpensesByMonthsByDayAction,
  getReportMovementsAction,
} from '../state/reports/actions';
import { useDispatch, useSelector } from 'react-redux';
import { ActivityIndicator, FlatList, Text } from 'react-native';

const Reports = () => {
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState(reportOptions[0]);
  const [selectedFilter, setSelectedFilter] = useState({});
  const [filters, setFilters] = useState([]);
  const [transactions, setTransactions] = useState([]);

  const expensesForecast = useSelector(
    (state) => state.reportsReducer.expensesForecast,
  );
  const transactionsByFilter = useSelector(
    (state) => state.reportsReducer.transactionsByFilter,
  );
  const fetchingExpensesForecast = useSelector(
    (state) => state.reportsReducer.fetchingExpensesForecast,
  );
  const fetchingTransactionsByFilter = useSelector(
    (state) => state.reportsReducer.fetchingTransactionsByFilter,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (!fetchingExpensesForecast && expensesForecast) {
      setLoading(false);
    }
  }, [fetchingExpensesForecast, expensesForecast]);

  useEffect(() => {
    const filters = transactionsByFilter.map((t) => {
      return { id: t.id, name: t.filter_text };
    });
    setFilters(filters);
    setSelectedFilter(filters[0]);
    setLoading(false);
  }, [transactionsByFilter]);

  useEffect(() => {
    const transactionFiltered = transactionsByFilter.find(
      (e) => e.id === selectedFilter.id,
    );
    if (transactionFiltered) {
      setTransactions(transactionFiltered.transactions);
    }
  }, [selectedFilter]);

  useEffect(() => {
    getExpensesForecast();
  }, []);

  const getExpensesForecast = () => {
    dispatch(getExpensesForecastAction());
  };

  const getExpensesByMonthsByCategory = () => {
    dispatch(getExpensesByMonthsByCategoryAction());
  };

  const getExpensesByMonthsByDay = () => {
    dispatch(getExpensesByMonthsByDayAction());
  };

  const getReportMovements = () => {
    dispatch(getReportMovementsAction());
  };

  const handleOnSelectOption = (item) => {
    setLoading(true);
    setSelected(item);
    switch (item.key) {
      case reportTypes.EXPENSES_FORECAST:
        getExpensesForecast();
        break;
      case reportTypes.EXPENSES_BY_MONTHS_BY_CATEGORY:
        getExpensesByMonthsByCategory();
        break;
      case reportTypes.EXPENSES_BY_MONTHS_BY_DAY:
        getExpensesByMonthsByDay();
        break;
      case reportTypes.MOVEMENTS:
        getReportMovements();
        break;
      default:
        break;
    }
  };

  const handleOnSelectFilter = (item) => {
    setSelectedFilter(item);
  };

  const renderExpenseForecastItem = (item) => (
    <ExpenseForecastItem item={item} />
  );

  const renderTransactionItem = (item) => <MovementItem item={item} />;

  const keyExtractorExpenseForecast = (item) => `${item.id}`;

  const keyExtractorTransaction = (item) => `${item.id}`;

  const renderNoDataToDisplay = () => {
    return !fetchingExpensesForecast && !fetchingTransactionsByFilter ? (
      <Text>No data to display.</Text>
    ) : null;
  };

  const renderList = () => {
    if (selected.key === reportTypes.EXPENSES_FORECAST) {
      return (
        <FlatList
          data={expensesForecast}
          keyExtractor={(item) => keyExtractorExpenseForecast(item)}
          renderItem={({ item }) => renderExpenseForecastItem(item)}
          ListEmptyComponent={() => renderNoDataToDisplay()}
        />
      );
    } else {
      return (
        <FlatList
          data={transactions}
          keyExtractor={(item) => keyExtractorTransaction(item)}
          renderItem={({ item }) => renderTransactionItem(item)}
          ListEmptyComponent={() => renderNoDataToDisplay()}
        />
      );
    }
  };

  const renderResults = () => {
    return (
      <>
        {!(
          selected.key === reportTypes.EXPENSES_FORECAST ||
          selected.key === reportTypes.MOVEMENTS
        ) &&
          filters.length > 0 && (
            <MaterialSelect
              data={filters}
              value={selectedFilter.name}
              onSelect={handleOnSelectFilter}
            />
          )}
        {renderList()}
      </>
    );
  };

  return (
    <>
      <Header title="Reports" />
      <ScrollViewContainer>
        <MaterialSelect
          data={reportOptions}
          value={selected.name}
          onSelect={handleOnSelectOption}
        />
        {loading ? <ActivityIndicator /> : renderResults()}
      </ScrollViewContainer>
    </>
  );
};

export default Reports;
