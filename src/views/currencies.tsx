import React, { useEffect } from 'react';

import { FloatButton, Header, LisItem, ViewContainer } from '../components';
import { useDispatch, useSelector } from 'react-redux';
import { Alert, FlatList, Text } from 'react-native';
import {
  deleteCurrencyAction,
  getCurrenciesAction,
} from '../state/currency/actions';

export interface Currency {
  id: number;
  name: string;
  abreviature: string;
}

export interface Props {
  navigation: {
    navigate: (routeName: string, params?: {}) => void;
  };
}
const Currencies: React.FC<Props> = ({ navigation: { navigate } }) => {
  const currencies = useSelector((state) => state.currencyReducer.currencies);
  const fetchingCurrencies = useSelector(
    (state) => state.currencyReducer.fetchingCurrencies,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    getCurrencies();
  }, []);

  const getCurrencies = () => {
    dispatch(getCurrenciesAction());
  };

  const deleteCurrency = (id: number) => {
    dispatch(deleteCurrencyAction(id));
    getCurrencies();
  };

  const handleDeleteCurrency = (id: number) => {
    Alert.alert('Delete currency', 'Are you sure?', [
      {
        text: 'Cancel',
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'Ok',
        onPress: () => deleteCurrency(id),
      },
    ]);
  };

  const handleEditCurrency = (item: Currency) => {
    navigate('EditCurrency', { currency: item });
  };

  const renderItem = (item: Currency) => (
    <LisItem
      item={item}
      onPressDelete={() => handleDeleteCurrency(item.id)}
      onPressEdit={() => handleEditCurrency(item)}
    />
  );

  const keyExtractor = (item: Currency) => `${item.id}`;

  const handleNewCurrency = () => {
    navigate('NewCurrency');
  };

  return (
    <>
      <Header title="Currencies" />
      <ViewContainer>
        <FlatList
          data={currencies}
          keyExtractor={(item) => keyExtractor(item)}
          renderItem={({ item }) => renderItem(item)}
          ListEmptyComponent={() => {
            return !fetchingCurrencies ? (
              <Text>No data to display.</Text>
            ) : null;
          }}
          refreshing={fetchingCurrencies}
          onRefresh={() => getCurrencies()}
        />
      </ViewContainer>
      <FloatButton onPress={handleNewCurrency} />
    </>
  );
};

export default Currencies;
