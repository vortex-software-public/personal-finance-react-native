import React, { useEffect, useState } from 'react';

import { Header } from '../components';
import CategoryForm from '../components/forms/categoryForm';
import { NavigationInjectedProps } from 'react-navigation';
import { useDispatch, useSelector } from 'react-redux';
import {
  resetInfoAction,
  updateCategoryAction,
} from '../state/category/actions';

export interface Props extends NavigationInjectedProps {}

const EditMovement: React.FC<Props> = ({
  navigation: { getParam, goBack },
}) => {
  const [categoryData, setCategoryData] = useState({
    name: '',
    nameError: '',
  });

  const updatingCategory = useSelector(
    (state) => state.categoryReducer.updatingCategory,
  );
  const successUpdatingCategory = useSelector(
    (state) => state.categoryReducer.successUpdatingCategory,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successUpdatingCategory) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successUpdatingCategory]);

  const handleEditMovement = () => {
    const data = {
      name: categoryData.name,
    };
    dispatch(updateCategoryAction(data));
  };

  useEffect(() => {
    const category = getParam('category');
    setCategoryData(category);
  }, []);

  return (
    <>
      <Header title="Edit Category" showGoBackButton />
      <CategoryForm
        categoryData={categoryData}
        setCategoryData={setCategoryData}
        onPressSave={handleEditMovement}
        loading={updatingCategory}
      />
    </>
  );
};

export default EditMovement;
