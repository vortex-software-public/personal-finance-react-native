import React, { useEffect, useState } from 'react';

import { Header } from '../components';
import CategoryForm from '../components/forms/categoryForm';
import { useDispatch, useSelector } from 'react-redux';
import {
  createCategoryAction,
  resetInfoAction,
} from '../state/category/actions';
import { NavigationInjectedProps } from 'react-navigation';

export interface Props extends NavigationInjectedProps {}

const NewCategory: React.FC<Props> = ({ navigation: { goBack } }) => {
  const [categoryData, setCategoryData] = useState({
    name: '',
    nameError: '',
  });

  const creatingCategory = useSelector(
    (state) => state.categoryReducer.creatingCategory,
  );
  const successCreatingCategory = useSelector(
    (state) => state.categoryReducer.successCreatingCategory,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successCreatingCategory) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successCreatingCategory]);

  const handleCreateCurrency = () => {
    const data = {
      name: categoryData.name,
    };
    dispatch(createCategoryAction(data));
  };

  return (
    <>
      <Header title="New Category" showGoBackButton />
      <CategoryForm
        categoryData={categoryData}
        setCategoryData={setCategoryData}
        onPressSave={handleCreateCurrency}
        loading={creatingCategory}
      />
    </>
  );
};

export default NewCategory;
