import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationInjectedProps } from 'react-navigation';

import { Header } from '../components';

import MovementForm from '../components/forms/movementForm';
import {
  createMovementAction,
  resetInfoAction,
} from '../state/movements/actions';

export interface Props extends NavigationInjectedProps {}

const NewMovement: React.FC<Props> = ({ navigation: { goBack } }) => {
  const [movementData, setMovementData] = useState({
    type: '',
    category: '',
    date: '',
    amount: '',
    currency: '',
    title: '',
    description: '',
    titleError: '',
    descriptionError: '',
    amountError: '',
  });

  const creatingMovement = useSelector(
    (state) => state.movementsReducer.creatingMovement,
  );
  const successCreatingMovement = useSelector(
    (state) => state.movementsReducer.successCreatingMovement,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (successCreatingMovement) {
      dispatch(resetInfoAction());
      goBack();
    }
  }, [successCreatingMovement]);

  const handleCreateCurrency = () => {
    const data = {
      title: movementData.title,
      type: movementData.type,
      category: movementData.category,
      date: movementData.date,
      amount: movementData.amount,
      description: movementData.description,
      currency: movementData.currency,
    };
    dispatch(createMovementAction(data));
  };

  return (
    <>
      <Header title="New Movement" showGoBackButton />
      <MovementForm
        movementData={movementData}
        setMovementData={setMovementData}
        onPressSave={handleCreateCurrency}
        loading={creatingMovement}
      />
    </>
  );
};

export default NewMovement;
