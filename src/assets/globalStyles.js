import { Platform, StyleSheet } from 'react-native';
import colors from './colors';

const globalStyles = StyleSheet.create({
  shadow: {
    ...Platform.select({
      android: {
        elevation: 3,
      },
      ios: {
        shadowColor: colors.SHADOW,
        shadowOffset: {
          height: 2,
          width: 0,
        },
        shadowOpacity: 0.5,
      },
    }),
  },
});

export default globalStyles;
