const doneIcon = require('./images/done.png');
const visibilityIcon = require('./images/visibility.png');
const visibilityOffIcon = require('./images/visibility_off.png');
const logo = require('./images/logo.png');
const currenciesIcon = require('./images/currencies.png');
const clientsIcon = require('./images/clients.png');
const categoriesIcon = require('./images/categories.png');
const editIcon = require('./images/edit.png');
const trashIcon = require('./images/trash.png');
const addIcon = require('./images/add.png');
const arrowBackIcon = require('./images/arrow_back.png');
const balanceIcon = require('./images/balance.png');
const reportsIcon = require('./images/reports.png');
const newMovementIcon = require('./images/new_movement.png');
const profileIcon = require('./images/profile.png');
const arrowDownIcon = require('./images/arrow_down.png');
const arrowBalanceIcon = require('./images/arrow_balance.png');

export {
  doneIcon,
  visibilityIcon,
  visibilityOffIcon,
  logo,
  currenciesIcon,
  clientsIcon,
  categoriesIcon,
  editIcon,
  trashIcon,
  addIcon,
  arrowBackIcon,
  balanceIcon,
  reportsIcon,
  newMovementIcon,
  profileIcon,
  arrowDownIcon,
  arrowBalanceIcon,
};
