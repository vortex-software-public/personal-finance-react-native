const colors = {
  PRIMARY_COLOR: '#6200EE',
  BLUE: '#4F00E5',
  WHITE: '#FFFFFF',
  BLACK_TEXT: '#000000',
  GRAY_TEXT: '#4F4F4F',
  LIGHT_GRAY_TEXT: '#828282',
  ERROR: '#F40000',
  GREEN: '#008464',
  SHADOW: '#1f1f1f',
};

export default colors;
