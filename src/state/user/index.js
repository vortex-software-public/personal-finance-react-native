import actionTypes from './types';

const initialState = {
  fetchingLogin: false,
  token: '',
  isAdmin: false,
  error: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCHING_LOGIN: {
      return {
        ...state,
        fetchingLogin: true,
        error: '',
      };
    }

    case actionTypes.FETCHING_LOGIN_SUCCESS: {
      const { token, isAdmin } = action.payload;
      return {
        ...state,
        fetchingLogin: false,
        token,
        isAdmin,
        error: '',
      };
    }

    case actionTypes.FETCHING_LOGIN_ERROR: {
      return {
        ...state,
        fetchingLogin: false,
        error: action.payload,
      };
    }

    default:
      return state;
  }
};
