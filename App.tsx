/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import { Provider } from 'react-redux';
import { YellowBox } from 'react-native';

import Router from './src/navigation/index';
import store from './src/state';

// TODO: Remove when fixed https://github.com/GeekyAnts/NativeBase/issues/2947 or search other solution
YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);

const App = () => {
  return (
    <Provider store={store}>
      <Router />
    </Provider>
  );
};

export default App;
